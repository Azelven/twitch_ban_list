# Ban List

### List of bots, dubious people or illegal nicknames to block & ban.

A ban list with pseudonyms/nicknames (bots or real persons) that includes hate raids, fake bot viewers, those who adhere to questionable morals/ethics and those who go beyond the rules of services or even laws/decrees/articles depending on their respective countries.

---

ℹ️ If you want to use my ban list with Nagia's script, you have a [wiki page](https://codeberg.org/Azelven/ban_list/wiki/Using-my-list-with-Nagia%27s-script) that explains in detail & step by step.